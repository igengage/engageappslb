package com.ige.configservice.config.service;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ige.configservice.dto.ConfigData;
@Service
public class ConfigService {
	@Value("${spring.cloud.config.server.git.uri}")
	String repoPath;
	
	public String updateConf(ConfigData configInput) throws Exception{
		PropertiesConfiguration properties = new PropertiesConfiguration(repoPath+"/application.properties");
		properties.setProperty(configInput.getKey(), configInput.getValue());
        properties.save();
		return "OK";
	}
}
