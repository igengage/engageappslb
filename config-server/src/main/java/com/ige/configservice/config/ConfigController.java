package com.ige.configservice.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.config.server.environment.MultipleJGitEnvironmentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ige.configservice.config.service.ConfigService;
import com.ige.configservice.dto.ConfigData;

@RestController
public class ConfigController {
	private static final Logger logger = LoggerFactory.getLogger(ConfigController.class);
	@Autowired
	ConfigService configService;

	@RequestMapping(value = "/config", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> reconfigure(@RequestBody ConfigData input) throws Exception {
		String retVal = "Error occured";
		try {
			retVal = configService.updateConf(input);
		} catch (Exception e) {
			logger.error("There is some error occured: ", e);
		}
		return new ResponseEntity<>("{\"msg\":\"" + retVal + "\"}", HttpStatus.OK);
	}
}