package com.ige.configservice.dto;

public class ConfigData {
	String key,value,comment,user;

	public String getKey() {
		return key;
	}
	

	public void setKey(String key) {
		this.key = key;
	}
	

	public String getValue() {
		return value;
	}
	

	public void setValue(String value) {
		this.value = value;
	}
	

	public String getComment() {
		return comment;
	}
	

	public void setComment(String comment) {
		this.comment = comment;
	}
	

	public String getUser() {
		return user;
	}
	

	public void setUser(String user) {
		this.user = user;
	}
	
}
