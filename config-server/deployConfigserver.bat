mkdir %1
set cDir=%cd%
echo %1
echo %cDir%
copy %cDir%\src\main\resources\config-repo\application.properties  %1%\application.properties
 cd %1
 git init 
 git add application.properties
 git commit -m "Initial commit"
 cd %cDir%
call mvn package 
echo %1
java -jar target\config-service-0.0.1-SNAPSHOT.jar --spring.cloud.config.server.git.uri=file:///%1


