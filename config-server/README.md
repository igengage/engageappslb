# README #

### What is this repository for? ###
This application is designed to be a config server based on spring cloud config server project.
it using file based config server backend, that is any change to the file will reflect to applications properties.

for that we need to call 
http://localhost:8888/config/

curl -X POST \
  http://localhost:8888/config/ \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 666d9622-e88f-4d92-e1e6-f8f2fc952fe1' \
  -d '{	"key":"abc","value":"11","comment":"Updated","user":"Rajaram"	}'

for our use case it is being called by load balancer.

### What is this repository for? ###

Change the spring.cloud.config.server.git.uri property in application.properties file to point the git folder
#### follow the deployConfigServer.bat #### 




### How do I get set up? ###
#### for set in IDE import as Maven project ####
#### for set in IDE import as Maven project ####

#### for Deployment follow the deployConfigServer.bat #### 

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact